#include "pgm.hpp"
#include <iostream>

using namespace std;

Pgm::Pgm(){}

Pgm::Pgm(string path){
	this->abrirImagem(path);
}

Pgm::~Pgm(){}

void Pgm::setDeslocamento(int deslocamento){
	this->deslocamento=deslocamento;
}

int Pgm::getDeslocamento(){
	return deslocamento;
}

void Pgm::lerImagem(){
	setNumeroMagico(retornarValoresDaImagem());
	getImagem().seekg(2+getImagem().tellg());
	setPixelInicioMensagem(stoi(retornarValoresDaImagem()));
	setTamanhoMensagem(stoi(retornarValoresDaImagem()));
	setDeslocamento(stoi(retornarValoresDaImagem()));
	setLargura(stoi(retornarValoresDaImagem()));
	setAltura(stoi(retornarValoresDaImagem()));
	setValorMaximoTonalidade(stoi(retornarValoresDaImagem()));
	getImagem().seekg(1+getImagem().tellg());
	setDimensao(1);

	definirTamanhoAtributoPixels(getLargura(),getAltura(),getDimensao());

	int totalPixels=getAltura()*getLargura();
	for(int pixel=0;pixel<totalPixels;pixel++)
		setPixel(getImagem().get(),pixel);
}