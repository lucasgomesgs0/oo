#include "imagem.hpp"
#include <iostream>

using namespace std;

Imagem::Imagem(){}

Imagem::Imagem(string path){
	this->abrirImagem(path);
}

Imagem::~Imagem(){
	imagem.close();
	delete pixels;
}

void Imagem::abrirImagem(string path){
	this->imagem.open(path);
	if(!imagem)
		throw ("\nErro ao tentar abrir a imagem");
}

ifstream& Imagem::getImagem(){
	return imagem;
}

string Imagem::retornarValoresDaImagem(){
	string variavelAuxiliar;
	imagem >> variavelAuxiliar;

	return variavelAuxiliar;
}

void Imagem::setNumeroMagico(string numeroMagico){
	this->numeroMagico=numeroMagico;
}

string Imagem::getNumeroMagico(){
	return numeroMagico;
}

void Imagem::setPixelInicioMensagem(int pixelInicioMensagem){
	this->pixelInicioMensagem=pixelInicioMensagem;
}

int Imagem::getPixelInicioMensagem(){
	return pixelInicioMensagem;
}

void Imagem::setTamanhoMensagem(int tamanhoMensagem){
	this->tamanhoMensagem=tamanhoMensagem;
}

int Imagem::getTamanhoMensagem(){
	return tamanhoMensagem;
}

void Imagem::setLargura(int largura){
	this->largura=largura;
}

int Imagem::getLargura(){
	return largura;
}

void Imagem::setAltura(int altura){
	this->altura=altura;
}

int Imagem::getAltura(){
	return altura;
}

void Imagem::setValorMaximoTonalidade(int valorMaximoTonalidade){
	this->valorMaximoTonalidade=valorMaximoTonalidade;
}

int Imagem::getValorMaximoTonalidade(){
	return valorMaximoTonalidade;
}

void Imagem::setDimensao(int dimensao){
	this->dimensao=dimensao;
}

int Imagem::getDimensao(){
	return dimensao;
}

void Imagem::setPixels(unsigned char *pixels){
	this->pixels=pixels;
}

unsigned char *Imagem::getPixels(){
	return pixels;
}

void Imagem::setPixel(unsigned char pixel,int posicao){
	this->pixels[posicao]=pixel;
}

unsigned char Imagem::getPixel(int pixel){
	return pixels[pixel];
}

void Imagem::definirTamanhoAtributoPixels(int largura,int altura,int dimensao){
	this->pixels=new unsigned char[largura*altura*dimensao];
}

// void Imagem::lerImagem(){}

void Imagem::salvarCopiaDaImagem(string fraseCriptografada,string fraseDescriptografada){
	int escolha;
	string pixelsString;

	cout << "\nDeseja salvar uma cópia da imagem com a frase criptografada e a frase descriptografada? Digite 1 para SIM e 0 para NAO. ";
	cin >> escolha;

	while(escolha!=0 && escolha!=1){
		cout << "Opção incorreta." << endl;
		cout << "\nDeseja salvar uma cópia da imagem com a frase criptografada e a frase descriptografada? Digite 1 para SIM e 0 para NAO. ";
		cin >> escolha;
	}

	switch(escolha){
		case 0:
			cout << "Programa finalizado" << endl;		
			break;
		case 1:
			ofstream copia;
			string nomeArquivo;
			
			cout << "Digite o nome do arquivo copia: ";
			cin >> nomeArquivo;

			if(numeroMagico=="P5")
				copia.open("img/"+nomeArquivo+".pgm");
			else if(numeroMagico=="P6")
				copia.open("img/"+nomeArquivo+".ppm");

			copia << numeroMagico << endl;
			copia << "# " << fraseCriptografada << endl;
			copia << "# " << fraseDescriptografada << endl;
			copia << largura << ' ' << altura << endl;
			copia << valorMaximoTonalidade << endl;

			for(int i=0;i<largura*altura*dimensao;i++){
				pixelsString+=getPixel(i);
			}

			copia << pixelsString;
			
			cout << "\nCópia da imagem foi salva dentro da pasta img no diretório raiz do projeto." << endl;
			cout << "Programa finalizado" << endl;

			copia.close();
			break;
	}
}
