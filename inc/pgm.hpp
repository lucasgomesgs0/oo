#ifndef PGM_HPP
#define PGM_HPP

#include "imagem.hpp"

class Pgm : public Imagem{
private:
	int deslocamento;
public:
	Pgm();
	Pgm(string path);
	~Pgm();
	
	void setDeslocamento(int deslocamento);
	int getDeslocamento();
	void lerImagem();
};

#endif