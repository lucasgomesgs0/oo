#ifndef CRIPTOGRAFIA_HPP
#define CRIPTOGRAFIA_HPP

#include <iostream>
#include <string>

using namespace std;

class Criptografia{
private:
	string fraseCriptografada;
	string fraseDescriptografada;
public:
	Criptografia();
	~Criptografia();

	void setFraseCriptografada(string );
	string getFraseCriptografada();
	void setCaracterCriptografado(char );
	char getCaracterCriptografado(int );
	
	void setFraseDescriptografada(string );
	string getFraseDescriptografada();

	void retirarMensagemCriptografadaDaImagem(int ,int ,int,unsigned char *);

	void descriptografar(string ,int );
	void descriptografar(string ,string );
	
};

#endif