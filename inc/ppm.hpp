#ifndef PPM_HPP
#define PPM_HPP

#include "imagem.hpp"

class Ppm : public Imagem{
private:
	string palavraChave;
public:
	Ppm();
	Ppm(string path);
	~Ppm();

	void setPalavraChave(string palavraChave);
	string getPalavraChave();

	void lerImagem();
};

#endif