#include "criptografia.hpp"

#include <string>

using namespace std;

Criptografia::Criptografia(){}
Criptografia::~Criptografia(){}

void Criptografia::setFraseCriptografada(string fraseCriptografada){
	this->fraseCriptografada=fraseCriptografada;
}
string Criptografia::getFraseCriptografada(){
	return fraseCriptografada;
}

void Criptografia::setFraseDescriptografada(string fraseDescriptografada){
	this->fraseDescriptografada=fraseDescriptografada;
}
string Criptografia::getFraseDescriptografada(){
	return fraseDescriptografada;
}
 
void Criptografia::setCaracterCriptografado(char caracterCriptografada){
	this->fraseCriptografada+=caracterCriptografada;
}
char Criptografia::getCaracterCriptografado(int posicao){
	return fraseCriptografada[posicao];
}

void Criptografia::retirarMensagemCriptografadaDaImagem(int pixelInicioMensagem,int tamanhoMensagem,int tipoDeCriptografia,unsigned char *pixels){
	switch(tipoDeCriptografia){	
		case 0: //Crifra de Cesar
			for(int i=pixelInicioMensagem;i<pixelInicioMensagem+tamanhoMensagem;i++)
				fraseCriptografada+=pixels[i];
			break;
		case 1: //Keyword Cipher
			char alfabeto[]={' ','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
			int posicaoAlfabeto;
			for(int i=pixelInicioMensagem;i<pixelInicioMensagem+(3*tamanhoMensagem);i+=3){
				posicaoAlfabeto=pixels[i]%10 + pixels[i+1]%10 + pixels[i+2]%10;
				fraseCriptografada+=alfabeto[posicaoAlfabeto];
			}
			break;
	}
}

void Criptografia::descriptografar(string fraseCriptografada,int deslocamento){
	char valorChar;
	fraseDescriptografada = fraseCriptografada;
	
	for(unsigned int i=0;i<fraseDescriptografada.size();i++){
	 	valorChar=fraseDescriptografada[i]-deslocamento;

		if((fraseDescriptografada[i]>=65 && fraseDescriptografada[i]<=90)||(fraseDescriptografada[i]>=97 && fraseDescriptografada[i]<=122)){
			if(valorChar<65){
			 	fraseDescriptografada[i]+=26;
			} else if (valorChar<97 && (fraseDescriptografada[i]>=97 && fraseDescriptografada[i]<=122)){
				fraseDescriptografada[i]+=26;
			}
			fraseDescriptografada[i]-=deslocamento;
		}
	}
}

void Criptografia::descriptografar(string fraseCriptografada,string palavraChave){
	
	char alfabeto[]={' ','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	char alfabetoKeyword[27];
	bool letraEstaNoAlfabeto=false;
	int posicaoAlfabetoKeyword=palavraChave.size()+1;
	
	alfabetoKeyword[0]=' ';
	for(unsigned int i=1;i<=palavraChave.size();i++){
		alfabetoKeyword[i]=toupper(palavraChave[i-1]);
	}

	for(int i=1; i<27 ;i++){
		for(unsigned int j=1;j<=palavraChave.size();j++){
			if(alfabeto[i]==alfabetoKeyword[j])
				letraEstaNoAlfabeto=true;
		}
		if(!letraEstaNoAlfabeto){
			alfabetoKeyword[posicaoAlfabetoKeyword]=alfabeto[i];
			posicaoAlfabetoKeyword++;
		}
		else{
			letraEstaNoAlfabeto=false;
		}
	}

	for(unsigned int i=0;i<fraseCriptografada.size();i++)
		for(int j=0;j<27;j++)
			if(fraseCriptografada[i]==alfabetoKeyword[j])
				fraseDescriptografada+=alfabeto[j];
}