#include "ppm.hpp"
#include <iostream>

using namespace std;

Ppm::Ppm(){}

Ppm::Ppm(string path){
	this->abrirImagem(path);
}

Ppm::~Ppm(){}

void Ppm::setPalavraChave(string palavraChave){
	this->palavraChave=palavraChave;
}

string Ppm::getPalavraChave(){
	return palavraChave;
}

void Ppm::lerImagem(){
	setNumeroMagico(retornarValoresDaImagem());
	getImagem().seekg(2+getImagem().tellg());
	setPixelInicioMensagem(stoi(retornarValoresDaImagem()));
	setTamanhoMensagem(stoi(retornarValoresDaImagem()));
	setPalavraChave(retornarValoresDaImagem());
	setLargura(stoi(retornarValoresDaImagem()));
	setAltura(stoi(retornarValoresDaImagem()));
	setValorMaximoTonalidade(stoi(retornarValoresDaImagem()));
	getImagem().seekg(1+getImagem().tellg());
	setDimensao(3);

	definirTamanhoAtributoPixels(getLargura(),getAltura(),getDimensao());

	int totalPixels=getAltura()*getLargura()*getDimensao();
	for(int i=0;i<totalPixels;i++)
		setPixel(getImagem().get(),i);
}