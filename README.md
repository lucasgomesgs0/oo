Aluno: Lucas Gomes Silva<br />
Matrícula: 16/0133505<br />
Disciplina: Orientação a Objetos<br />

# EP1

#### Prepação para iniciar o programa
Todos os comandos serão executados dentro do terminal e é recomendado colocar as imagem dentro da pasta _img_ localizada na raiz do projeto, assim facilitando o acesso ao local da imagem.

Após clonar o repositório, entrar na pasta raiz do projeto e executar o seguinte comando para limpar as pastas obj e bin:
```sh
$ make clean
```

Após terminar a execução, usar o seguinte comando para compilar o projeto:
```sh
$ make
```

E, finalmente, para iniciar o programa, executar o comando:
```sh
$ make run
```


#### Dentro do programa

O programa foi criado de forma a ter interação com o usuário. Já dentro do programa, aparecerá uma frase onde se pede para digitar 0 ou 1, onde:<br />
	__0__ : Sai do programa<br />
	__1__ : Inicia o programa

Após teclar ENTER, aparecerá outra frase onde se pede que o usuário também digite 0 ou 1 para selecionar a forma de abertura da imagem seja digitando apenas o nome ou digitando o caminho até a imagem, onde:<br />
	__0__ : Digitar o caminho até a imagem<br />
	__1__ : Digitar apenas o nome junto __com__ o tipo da imagem (.pgm/.ppm)

Ao digitar o nome da imagem ou o caminho até ela, aparecerá no terminal a frase criptografada encontrada dentro da imagem e a frase descriptografada. 

Logo após mostrar as frases, o usuário terá que escolher entre 0 ou 1 para selecionar se quer salvar uma cópia da imagem junto com as frases dentro da imagem como comentário ou não são salvar e assim finalizando o programa, onde:<br />
	__0__ : Finaliza o programa<br />
	__1__ : Salva o programa

Se o usuário digitar 1, será necessário digitar o nome do arquivo __sem__ o formato da imagem (.ppm/.ppm) e a copia da imagem será salvo dentro da pasta _img_, após isso o programa será finalizado.  

#### Erro ao tentar abrir a imagem
Se ocorrer algum erro ao tentar abrir a imagem, seja por ter digitado o nome errado ou pelo arquivo não existir, irá apaparecer uma frase onde pergunta se o usuário quer ajuda para abrir a imagem. O usuário terá que digitar 0 ou 1, onde:<br />
	__0__ : Não quer ajuda para abrir a imagem<br />
	__1__ : Quer ajuda para abrir a imagem

Se o usuário digitar 0, o programa voltará ao inicio onde irá pedir se o usuário quer iniciar o programa. Caso o usuário digite 1, aparecerá um parágrafo explicando como abrir a imagem.