#include "imagem.hpp"
#include "pgm.hpp"
#include "ppm.hpp"
#include "criptografia.hpp"
#include <iostream>
#include <string>

using namespace std;

int main(int argc,char **argv){
	string path,nomeArquivo;
	int escolha,escolhaMeioDeAcessarImagem;

	cout << "Iniciar descriptografia? Digite 1 para SIM ou 0 para SAIR DO PROGRAMA. ";
	cin >> escolha;

	while(escolha != 0 && escolha != 1){
		cout << "\nOpção incorreta." << endl;
		cout << "Iniciar descriptografia? Digite 1 para SIM ou 0 para SAIR DO PROGRAMA. ";
		cin >> escolha;
	}

	switch(escolha){
		case 0:
			cout << "Programa encerrado." << endl;
			return 0;
		break;	
		case 1:
			cout << "\nDeseja digitar apenas o nome do arquivo ou o caminho até o arquivo? Digite 1 para DIGITAR APENAS O NOME e 0 PARA DIGITAR O CAMINHO. ";
			cin >> escolhaMeioDeAcessarImagem;

			while(escolhaMeioDeAcessarImagem != 0 && escolhaMeioDeAcessarImagem != 1){
				cout << "\nOpção incorreta." << endl;
				cout << "Deseja digitar apenas o nome do arquivo ou o caminho até o arquivo? Digite 1 para DIGITAR APENAS O NOME e 0 PARA DIGITAR O CAMINHO. ";
				cin >> escolhaMeioDeAcessarImagem;
			}

			switch(escolhaMeioDeAcessarImagem){
				case 0:
					cout << "Digite o caminho até a imagem: ";
					cin >> path;
				break;
				case 1:
					cout << "\nDigite apenas o nome da imagem junto com o tipo (.pgm/.ppm): ";
					cin >> nomeArquivo;
					path="img/"+nomeArquivo;
				break;
			}
			
			try{

			Imagem * imagemInfo = new Imagem(path);

				imagemInfo->setNumeroMagico(imagemInfo->retornarValoresDaImagem());
				

				if(imagemInfo->getNumeroMagico()=="P5"){
					delete imagemInfo;

					Pgm * imagem = new Pgm(path);

					imagem->lerImagem();

					Criptografia * criptografia = new Criptografia();
					criptografia->retirarMensagemCriptografadaDaImagem(imagem->getPixelInicioMensagem(),imagem->getTamanhoMensagem(),0,imagem->getPixels());
					criptografia->descriptografar(criptografia->getFraseCriptografada(),imagem->getDeslocamento());

					cout << "\nMensagem criptografada: \n" << criptografia->getFraseCriptografada() << endl;
					cout << "\nMensagem Descriptografada: \n" << criptografia->getFraseDescriptografada() << endl;

					imagem->salvarCopiaDaImagem(criptografia->getFraseCriptografada(),criptografia->getFraseDescriptografada());

					imagem->getImagem().close();
					delete criptografia;
					delete imagem;	
				}
				else if(imagemInfo->getNumeroMagico()=="P6"){
					delete imagemInfo;

					Ppm * imagem = new Ppm(path);

					imagem->lerImagem();

					Criptografia * criptografia = new Criptografia();
					criptografia->retirarMensagemCriptografadaDaImagem(imagem->getPixelInicioMensagem(),imagem->getTamanhoMensagem(),1,imagem->getPixels());
					criptografia->descriptografar(criptografia->getFraseCriptografada(),imagem->getPalavraChave());

					cout << "\nMensagem criptografada: \n" << criptografia->getFraseCriptografada() << endl;
					cout << "\nMensagem Descriptografada: \n" << criptografia->getFraseDescriptografada() << endl;

					imagem->salvarCopiaDaImagem(criptografia->getFraseCriptografada(),criptografia->getFraseDescriptografada());

					delete criptografia;
					delete imagem;	
				}
				
			}
			catch (const char *e){
				int escolhaAjuda;

				cout << e << endl;
				cout << "Quer ajuda para abrir a imagem? Digite 1 para SIM ou 0 para NÃO. ";
				cin >> escolhaAjuda;
				while(escolhaAjuda != 0 && escolhaAjuda != 1){
					cout << "\nOpção incorreta." << endl;
					cout << "Quer ajuda para abrir a imagem? Digite 1 para SIM ou 0 para NÃO. ";
					cin >> escolhaAjuda;
				}
				switch(escolhaAjuda){
					case 0:
						main(argc,argv);
						break;
					case 1:
						cout << endl;
						cout << "É recomendado colocar a imagem dentro da pasta img na raiz do projeto para facilitar o acesso as imagens." << endl;
						cout << "Digite apenas o nome da imagem, caso a imagem esteja dentro da pasta img." << endl;
						cout << "Caso a imagem esteja fora da pasta img, digite o caminho até a imagem junto com o tipo da imagem." << endl;
						main(argc,argv);
						break;
				}
			}
		break;
	}

  return 0;
}
