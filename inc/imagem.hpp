#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>
#include <fstream>

using namespace std;

class Imagem{
protected:
	
private:
	ifstream imagem;
	string numeroMagico;
	int pixelInicioMensagem;
	int tamanhoMensagem;
	string variavelParaDescriptografia;
	int largura;
	int altura;
	int valorMaximoTonalidade;
	int dimensao;
	unsigned char *pixels;
public:
	Imagem();
	Imagem(string path);
	~Imagem();
	
	void abrirImagem(string path);
	ifstream& getImagem();
	string retornarValoresDaImagem();

	void setNumeroMagico(string numeroMagico);
	string getNumeroMagico();

	void setPixelInicioMensagem(int pixelInicioMensagem);
	int getPixelInicioMensagem();

	void setTamanhoMensagem(int tamanhoMensagem);
	int getTamanhoMensagem();

	void setVariavelParaDescriptografia(string variavelParaDescriptografia);
	string getVariavelParaDescriptografia();

	void setLargura(int largura);
	int getLargura();

	void setAltura(int altura);
	int getAltura(); 

	void setValorMaximoTonalidade(int valorMaximoTonalidade);
	int getValorMaximoTonalidade(); 

	void setDimensao(int dimensao);
	int getDimensao();

	void setPixels(unsigned char *pixels);
	unsigned char *getPixels();
	void setPixel(unsigned char pixel);
	void setPixel(unsigned char pixel,int posicao);
	unsigned char getPixel(int pixel);

	void definirTamanhoAtributoPixels(int,int,int);

	void lerImagem();

	void salvarCopiaDaImagem(string,string);
};

#endif